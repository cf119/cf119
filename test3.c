#include<stdio.h>
#include<math.h>
int main()
{
	float a,b,c,d,r1,r2;
	printf("Enter the value of a,b and c of the quadratic equation:");
	scanf("%f%f%f",&a,&b,&c);
	if(a==0)
	{
		printf("Not a Quadratic equation\n");
		return 0;
	}
	else
	{
		d=(b*b)-4*a*c;
		
		if(d==0)
		{ 	
		    r1=-b/(2*a);
			r2=r1;
			printf("Roots are real and equal\n");
		}
		else if (d>0)
		{
			printf("Roots are real and distinct \n");
			r1=(-b+sqrt(d))/2*a;
			r2=(-b-sqrt(d))/2*a;
		}
		else if(d<0)
		{
			printf("Roots are imaginary\n");
			return 0;
		}
	}
	printf("Roots are %f and %f\n",r1,r2);
	return 0;
}
