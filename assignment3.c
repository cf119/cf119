#include<stdio.h>
void display(float);
int main()
{
	float m1,m2,m3,m4,m5,grade;
	printf("Enter the marks in 5 subjects");
	scanf("%f%f%f%f%f",&m1,&m2,&m3,&m4,&m5);
	grade=(m1+m2+m3+m4+m5)/5.0;
    if(m1<40||m2<40||m3<40||m4<40||m5<40)
    { 
        printf("Average is %f and the grade is F\n",grade);
        return 0;
    }
    else
        display(grade);
        return 0;
}
void display(float g)
{	
	printf("Average is %f and ",g);
    if(g>=90 && g<=100)
		printf("The Grade is S\n");
	if(g>=80 && g<90)
		printf("The Grade is A\n");
	if(g>=70 && g<80)
		printf("The Grade is B\n");
	if(g>=60 && g<70)
		printf("The Grade is C\n");
	if(g>=50 && g<60)
		printf("The Grade is D\n");
	if(g>=40 && g<50)
		printf("The Grade is E\n");
	if(g<40)
		printf("The Grade is F\n");
}
		